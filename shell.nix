{ pkgs ? import <nixpkgs> { } }:

with pkgs;

mkShell { buildInputs = [ python38 ]; }
